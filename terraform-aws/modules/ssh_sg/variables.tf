// Module specific variables
variable "name" {
  default = "test"
}

variable "environment" {
  default = "test"
}

variable "vpc_id" {
  description = "The VPC this security group will go in"
}

variable "opus_tallinn_ip" {
  description = "Tallinn Opus office IP"
  default     = "62.65.40.138/32"
}

variable "source_cidr_block" {
  description = "The source CIDR block to allow traffic from"
}
